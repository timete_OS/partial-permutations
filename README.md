Partial Permutation
=============

### What is it ?


A (dys)functional implementation of [arrangements](https://en.wikipedia.org/wiki/Combination) generation in Rust. I must say I am utterly annoyed by the fact English is calling such arrangements 'combinations'.
French has a much more convenient conventions:
 - Combinations are with repetition and ordering
 - Permutations are without repetition and with ordering
 - Arrangements are without repetition and without ordering
 - No one ever use combinations without ordering so there is no word for this



### How it works

The code bellow is not exactly the one used, but give an idea of how the actual function works.
```rust
pub fn arrangements(m: usize, n: usize, k: usize) -> Box<dyn Iterator<Item = Vec<usize>>> {
    match (n, k) {
        (0, _) => Box::new(std::iter::empty::<Vec<usize>>()),
        (_, 0) => Box::new(std::iter::empty::<Vec<usize>>()),
        (_, 1) => Box::new((0..n).map(move |x| vec![x])),
        (_, _) => {
            let with_last = arrangements(n - 1, k);
            let without = arrangements(n - 1, k - 1).map(move |mut perm| {
                perm.push(n - 1);
                perm
            });

            Box::new(without.chain(with_last))
        }
    }   
}
```
The `partial_permutation` function returns an iterator over vectors of $k$ indices of a set of size $n$.

The idea is rather simple. In order to generate all arangement of $k$ elements of a set of size $n$, the problem can be stated as:

- if the set is empty, return an empty set
- if $k=1$, return the indexes of the set
- else, all arangement of size $k-1$ when the last element is taken *and* all arangement of size $k$ when the last element is excluded

### How does it perform

#### Rust versions

You can test that with `cargo bench`. There are two version, one recursive and one iterative.

There are no tail recursion optimisation possible for the recursive version, so it performs roughly four times worse than the iterative version. Also, it does not return a proper iterator, as all the computation cost is made at creation (not lazy).


The iterative version is a simple state machine, so it should be as fast as hand-written assembly, I guess.

#### Recursion in Haskell

Since I found the recursive version a lot easier to write, I was bummed to see it perform so badly. I wanted to see how it would perform with a language designed for recursion, so I wrote it in Haskell, and again tried to benchmark it using criterion.

But there must be some mistake, because there is just **no way** it can enumerate 12 870 items (that are lists!) in 80*ns* on a 3 GHz CPU. For context, the Rust reference implementation in [itertools](https://crates.io/crates/itertools) took over **one millisecond** to achieve the same.


```
benchmarking arrangements/15
time                 79.63 ns   (78.54 ns .. 81.44 ns)
                     0.999 R²   (0.997 R² .. 1.000 R²)
mean                 79.29 ns   (78.72 ns .. 80.31 ns)
std dev              2.481 ns   (1.347 ns .. 3.807 ns)
```

Aaaaaaand yes, indeed, Haskell is so lazy that for some reasons I do not understand it would basically not run anything during benchmark. So I added some consummer function that would do actual computation with the result of this iterator, and here are the results.

**Haskell** :
```
benchmarking arrangements/15
time                 3.095 ms   (3.026 ms .. 3.180 ms)
                     0.994 R²   (0.989 R² .. 0.998 R²)
mean                 3.230 ms   (3.158 ms .. 3.411 ms)
std dev              357.8 μs   (155.2 μs .. 732.2 μs)
```


**Rust** :
```
general bench/reference 15
                        time:   [447.62 µs 450.70 µs 453.96 µs]
                        change: [-2.2462% -0.9655% +0.3484%] (p = 0.15 > 0.05)

general bench/iterative 15
                        time:   [347.31 µs 351.25 µs 355.81 µs]
                        change: [+13.358% +16.251% +18.923%] (p = 0.00 < 0.05)

Benchmarking general bench/recursive 15: Warming up for 3.0000 s
general bench/recursive 15
                        time:   [2.2575 ms 2.2973 ms 2.3366 ms]
                        change: [-6.1555% -4.1505% -2.1911%] (p = 0.00 < 0.05)
```


So even the recursive version in Rust is faster than the Haskell one. I'll admit that I learned just enough Haskell to be able to write this simple function. I am therefor not qualify to assert this is slow because of the language and not because of my poor implementation.

But it doesn't matter, as to my understanding the core philosophy of Haskell is writing code that is simple and readable, and that tweaking things for the sake of performance is not part of the ethos.