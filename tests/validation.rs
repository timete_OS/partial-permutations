use std::collections::HashSet;

use arrangements::{arrangements, arrangements_rec};

#[test]
fn simple_correct_enumeration() {
    let (n, k) = (5, 3);

    let itrs = arrangements(n, k);
    let hash_set = HashSet::from_iter(itrs.into_iter());
    let hash_set_rec: HashSet<Vec<usize>> = HashSet::from_iter(arrangements_rec(n, k).into_iter());

    let mut true_set = HashSet::new();
    true_set.insert(vec![0, 1, 2]);
    true_set.insert(vec![0, 1, 3]);
    true_set.insert(vec![0, 1, 4]);
    true_set.insert(vec![0, 2, 3]);
    true_set.insert(vec![0, 2, 4]);
    true_set.insert(vec![0, 3, 4]);
    true_set.insert(vec![1, 2, 3]);
    true_set.insert(vec![1, 2, 4]);
    true_set.insert(vec![1, 3, 4]);
    true_set.insert(vec![2, 3, 4]);
    
    assert_eq!(true_set, hash_set);
    assert_eq!(true_set, hash_set_rec);
}

#[test]
fn trivial_correct_enumeration() {
    let (n, k) = (1, 1);

    let hash_set = HashSet::from_iter( arrangements_rec(n, k).into_iter());
    let hash_set_rec: HashSet<Vec<usize>> = HashSet::from_iter(arrangements_rec(n, k).into_iter());
    
    let mut true_set = HashSet::new();
    true_set.insert(vec![0]);

    
    assert_eq!(true_set, hash_set);
    assert_eq!(true_set, hash_set_rec);
}

#[test]
fn also_trivial_correct_enumeration() {
    let (n, k) = (30, 1);

    let hash_set = HashSet::from_iter( arrangements_rec(n, k).into_iter());
    let hash_set_rec: HashSet<Vec<usize>> = HashSet::from_iter(arrangements_rec(n, k).into_iter());
    
    let mut true_set = HashSet::new();
    for i in 0..n {
        true_set.insert(vec![i]);
    }
    
    assert_eq!(true_set, hash_set);
    assert_eq!(true_set, hash_set_rec);
}

#[test]
fn invalid_enumeration() {
    let (n, k) = (30, 0);

    let hash_set: HashSet<Vec<usize>> = HashSet::from_iter( arrangements_rec(n, k).into_iter());
    let hash_set_rec: HashSet<Vec<usize>> = HashSet::from_iter(arrangements_rec(n, k).into_iter());
    
    let  true_set: HashSet<Vec<usize>> = HashSet::new();
    
    assert_eq!(true_set, hash_set);
    assert_eq!(true_set, hash_set_rec);
}

#[test]
#[should_panic]
fn should_panic_rec() {
    let (n, k) = (30, 51);
    let _it = arrangements_rec(n, k);
}

#[test]
#[should_panic]
fn should_panic_iter() {
    let (n, k) = (30, 51);
    arrangements(n, k).next();
}