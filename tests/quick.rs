#[cfg(test)]
#[macro_use]
extern crate quickcheck;


#[cfg(test)]
mod tests {

    use std::collections::HashSet;
    use arrangements::{arrangements, arrangements_rec, binomial};
    use quickcheck::TestResult;

    quickcheck! {
        fn correct_arrangement(n: usize, k: usize) -> TestResult {
    
            // The partial permutation of a is the only iterator that satisfy:
            //  - length of `binomial(n, k)`
            //  - all elements are unique and included in a
            //  - there are no repetition within elements of the iterator and they are all length k
            if n > 20 || k > n {
                return TestResult::discard()
            };
            let mut res = true;

            let values = HashSet::from_iter(0..n);

            let output_set: HashSet<Vec<usize>> = HashSet::from_iter(arrangements(n, k));
    
            if n != 0 && k != 0 {
                res = res && (binomial(n, k) == output_set.len());
            } else {
                res = res && 0 == output_set.len();
            }
    
            for arrangment in output_set {
                let items: HashSet<usize> = HashSet::from_iter(arrangment.iter().cloned());
                res = res && items.len() == k;
                res = res && arrangment.len() == k;
                res = res && items.is_subset(&values);
            };

            TestResult::from_bool(res)
        }
    }


    quickcheck! {
        fn correct_arrangemen_rec(n: usize, k: usize) -> TestResult {
            if n > 20 || k > n {
                return TestResult::discard()
            };
            let mut res = true;

            let values = HashSet::from_iter(0..n);

            let output_set: HashSet<Vec<usize>> = HashSet::from_iter(arrangements_rec(n, k));
    
            if n != 0 && k != 0 {
                res = res && (binomial(n, k) == output_set.len());
            } else {
                res = res && 0 == output_set.len();
            }
    
            for arrangment in output_set {
                let items: HashSet<usize> = HashSet::from_iter(arrangment.iter().cloned());
                res = res && items.len() == k;
                res = res && arrangment.len() == k;
                res = res && items.is_subset(&values);
            };

            TestResult::from_bool(res)
        }
    }

}