// use memoize::memoize;


pub fn binomial(n: usize, k: usize) -> usize {
    match (n, k) {
        (_, 0) => 1,
        (_, 1) => n,
        (n, k) if k > n / 2 => binomial(n, n - k),
        _ => n * binomial(n - 1, k - 1) / k
    }
} 



pub fn partial_permutation_optim(m: usize, n: usize, k: usize) -> Box<dyn Iterator<Item = Vec<usize>>> {
    match (n, k) {
        (0, _) => Box::new(std::iter::empty::<Vec<usize>>()),
        (_, 0) => Box::new(std::iter::empty::<Vec<usize>>()),
        (n, k) if k == n => {
            let mut r: Vec<usize> = Vec::with_capacity(m);
            (0..n).for_each(|i| r.push(i));
            Box::new(std::iter::once(r))
        },
        (n, k) if k > n => panic!("Cannot find {k} element in collection of length {n}"),
        (_, 1) => Box::new((0..n).map(move |x| {
            let mut r: Vec<usize> = Vec::with_capacity(m);
            r.push(x);
            r
        })),
        (_, _) => {
            let with_last = partial_permutation_optim(m, n - 1, k);
            let without = partial_permutation_optim(m, n - 1, k - 1).map(move |mut perm| {
                perm.push(n - 1);
                perm
            });

            Box::new(without.chain(with_last))
        }
    }   
}

pub fn arrangements_rec(n: usize, k: usize) -> Box<dyn Iterator<Item = Vec<usize>>> {
    partial_permutation_optim(k, n, k)
}



pub struct Arrangements {
    n: usize,
    k: usize,
    i: usize,
    j: usize,
    started: bool,
    indices: Vec<usize>
}

pub fn arrangements(n: usize, k: usize) -> Arrangements
{
    Arrangements {
        n,
        k,
        i: k.saturating_sub(1),
        j: k,
        started: false,
        indices: (0..k).collect()
    }
}

impl Iterator for Arrangements
{
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Vec<usize>> {
        if !self.started {
            self.started = true;
            return match (self.k, self.n) {
                (0, _) => None,
                (_, 0) => None,
                (k, n) if k > n => panic!("Cannot find {k} elements in a collection of length {n}"),
                (_, _) => Some(self.indices.clone())
            }
        }
        if self.indices[0] >= self.n - self.k {
            return None
        }
        if self.indices[self.i] < self.n - 1 {
            self.indices[self.i] += 1;
            Some(self.indices.clone())
        } else {
            let mut index = self.i;
            while self.indices[index] == self.indices[index-1] + 1 {
                index -= 1;
            }

            self.j = index - 1;

            self.indices[self.j] += 1;
            let mut p = self.indices[self.j];
            for value in self.indices[index..].iter_mut() {
                p += 1;
                *value = p;
            }
            self.i = self.k-1;

            Some(self.indices.clone())
        }
    }
}


// pub fn extend_partial_permutation(previous_iterator:  Box<dyn Iterator<Item = Vec<usize>>>, p:usize, n: usize, k: usize) -> Box<dyn Iterator<Item = Vec<usize>>> {

//     let d = n.saturating_sub(p);

//     match (d, k) {
//         (0, _) => previous_iterator,
//         (1, _) => {
//             let ne = previous_iterator.map(move |mut x| {
//                 x.push(p - 1);
//                 x
//             });

//             Box::new(previous_iterator.chain(ne))

//         },
//         (_, _) => {
//             // let ne = extend_partial_permutation(previous_iterator, p, n, k);
//             extend_partial_permutation(extend_partial_permutation(previous_iterator, p, n - 1, k), p, n, k)
//         }
//     }

// }





pub fn arrangment_table(n: usize, k: usize) -> Vec<Vec<bool>> {
    let l = binomial(n, k);
    
    let mut table = vec![vec![false; n]; l];

    for (row, indices) in arrangements(n, k).enumerate() {
        indices.iter().for_each(|&x| table[row][x] = true);
    }

    table
}
