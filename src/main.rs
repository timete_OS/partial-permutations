use arrangements::*;


fn main() {


    let (n, k) = (5, 5);
    let mut perms = arrangements(n, k);
    println!("{:p}", &&perms);

    
    // let size_of_vec = 2;
    // let ptr = Box::into_raw(perms) as *const u8;
    // let final_adress = ptr.wrapping_add(binomial(n, k) * size_of_vec);
    
    
    // println!("supposed final address{:p}", final_adress);

    while match perms.next() {
        Some(g) => {
            println!("{:?} at address {:p}", g, &&g);
            true
        },
        None => false
    } {
        // println!("{:p}", &&perms);
    }


}
