import Criterion.Main

arrangements :: Int -> Int -> [[Int]]
arrangements n k
    | k == 0    = []
    | n == 0    = []
    | k == 1    = map (\x -> [x]) [0..n-1]
    | k == n    = [[0..n-1]]
    | otherwise = withoutLast ++ withLast
  where
    withLast = map (\perm -> perm ++ [n-1]) (arrangements (n-1) (k-1))
    withoutLast = arrangements (n-1) k


likelihood :: [Int] -> Int
likelihood permut = do
  let values =[3, 4, 1, 1, 0, 0, 4, 8, 3, 1, 3, 2, 4, 0, 3, 3, 2]
  foldl (\acc (a, b) -> acc + a*b) 0 (zip values permut)

arrangements_bench :: Int -> Int
arrangements_bench m =
    foldl (\acc x -> acc + x) 0 (map (\x -> likelihood x) (arrangements n k))
  where
    n = m + 1
    k = m `div` 2

-- main :: IO ()
-- main = do
--     let m = 8
--         result = arrangements_bench m

--     print result

main :: IO ()
main = defaultMain [
  bgroup "arrangements" [ bench "1"  $ whnf arrangements_bench 1
               , bench "5"  $ whnf arrangements_bench 5
               , bench "9"  $ whnf arrangements_bench 9
               , bench "15" $ whnf arrangements_bench 15
               ]
  ]