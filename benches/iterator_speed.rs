use std::time::Duration;

use arrangements::{arrangements, arrangements_rec};
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use itertools::Itertools;

fn likelihood(perm : Vec<usize>) -> usize {
    let values = [3, 4, 1, 1, 0, 0, 4, 8, 3, 1, 3, 2, 4, 0, 3, 3, 2];
    perm.iter().zip(values).fold(0, |acc, (x, y)| acc + x * y)
}

fn iterative_benchmark(c: &mut Criterion) {

    let mut long_benches = c.benchmark_group("general bench");
    long_benches.measurement_time(Duration::new(10, 0));

    let consumer = |n: usize| {
        let n = n + 1;
        let k = n / 2;
        let it = arrangements(n, k);
        it.map(likelihood).fold(0, |acc, x| acc + x)
    };

    long_benches.bench_function("iterative 15", |b| b.iter(|| consumer(black_box(15))));
    long_benches.finish();
}


fn reference_benchmark(c: &mut Criterion) {

    let mut long_benches = c.benchmark_group("general bench");
    long_benches.measurement_time(Duration::new(10, 0));

    let consumer = |n: usize| -> usize {
        let n = n + 1;
        let k = n / 2;
        let it = (0..n).combinations(k);
        it.map(likelihood).fold(0, |acc, x| acc + x)
    };

    long_benches.bench_function("reference 15", |b| b.iter(|| consumer(black_box(15))));
    long_benches.finish();
}


fn recursive_benchmark(c: &mut Criterion) {

    let mut long_benches = c.benchmark_group("general bench");
    long_benches.measurement_time(Duration::new(10, 0));
    
    let consumer = |n: usize| -> usize {
        let n = n + 1;
        let k = n / 2;
        let it = arrangements_rec(n, k);
        it.map(likelihood).fold(0, |acc, x| acc + x)
    };

    long_benches.bench_function("recursive 15", |b| b.iter(|| consumer(black_box(15))));
    long_benches.finish();
}


fn trivial_enumeration_iter(c: &mut Criterion) {
    
    let consumer = |n: usize| {
        let n = n;
        let k = 1;
        let it = arrangements(n, k);
        it.map(likelihood).fold(0, |acc, x| acc + x)
    };

    c.bench_function("iterative trivial 50", |b| b.iter(|| consumer(black_box(50))));
}


fn trivial_enumeration_rec(c: &mut Criterion) {
    
    let consumer = |n: usize| {
        let n = n;
        let k = 1;
        let it = arrangements_rec(n, k);
        it.map(likelihood).fold(0, |acc, x| acc + x)
    };

    c.bench_function("recursive trivial 50", |b| b.iter(|| consumer(black_box(50))));
}


criterion_group!(benches, reference_benchmark, iterative_benchmark, recursive_benchmark, trivial_enumeration_iter, trivial_enumeration_rec);
criterion_main!(benches);